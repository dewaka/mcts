#include <vector>
#include <string>
#include <iostream>
#include "parse_in_file.hpp"

using namespace std;

int* hill_climbing()
{
	arg_range *arg_value_ranges = nullptr;
	int n = mapInfo.nr_variable_params;
	int *arg_values = new int[n];
	int *best_values = new int[n];
	double current_best;

	init_arg_ranges(arg_value_ranges);
	for (int i=0; i<n; i++) {
		arg_values[i] = arg_value_ranges[i].loBound;
	}

	// choose a random point in search space and evaluate it
	for (int j=0; j<n; j++) {
		int range = (arg_value_ranges[j].hiBound - arg_value_ranges[j].loBound) / arg_value_ranges[j].step;
		int randStep = rand() % arg_value_ranges[j].step;
		arg_values[j] = arg_value_ranges[j].loBound + randStep * arg_value_ranges[j].step;
	}

	current_best = setArgAndRun(arg_values);
	copy_best_values (n, best_values, arg_values);

	bool climbFinished = false;

	for (int ind=0; ind<n; ind++) {
		// choose the direction for parameter ind
		int direction = 0;
		double eval;
		arg_values[ind] += arg_value_ranges[ind].step;
		if (arg_values[ind] > arg_value_ranges[ind].hiBound)
			arg_values[ind] -= arg_value_ranges[ind].step;
		else {
			eval = setArgAndRun(arg_values);
			if (eval < current_best) {
				current_best = eval;
				copy_best_values(n, best_values, arg_values);
				direction = 1;
			}
			arg_values[ind] -= arg_value_ranges[ind].step;
		}
		arg_values[ind] -= arg_value_ranges[ind].step;
		if (arg_values[ind] < arg_value_ranges[ind].loBound)
			arg_values[ind] += arg_value_ranges[ind].step;
		else {
			eval = setArgAndRun(arg_values);
			if (eval < current_best) {
				current_best = eval;
				copy_best_values(n, best_values, arg_values);
				direction = -1;
			}
			arg_values[ind] += arg_value_ranges[ind].step;
		}

		if (direction == 0)
			continue;
		else {
			arg_values[ind] += direction * arg_value_ranges[ind].step;
			bool climb = true;
			while (climb) {
				arg_values[ind] += direction * arg_value_ranges[ind].step;
				eval = setArgAndRun(arg_values);
				if (eval < current_best) {
					current_best = eval;
					copy_best_values(n, best_values, arg_values);
				} else
					climb = false;
			}
		}
	}

}
