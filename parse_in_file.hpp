#ifndef RP_PARSE_IN_FILE_H
#define RP_PARSE_IN_FILE_H

#include <string>
#include "mapping_info.hpp"

void parse_in_file(std::string fileName);

int parse_in_file(const std::string &fileName, map_info &info);

double setArgAndRun (int arg_val[]);

void init_arg_ranges(arg_range a[]);

void copy_best_values(int n, int best_values[], int arg_values[]);

#endif // RP_PARSE_IN_FILE_H
