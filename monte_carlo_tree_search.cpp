#include <iostream>
#include <string>
#include <vector>

#include "parse_in_file.hpp"

map_info mapInfo;

using namespace std;

/*
Search procedure:
function search(position, depth)
begin
  if GameOver then
    return GameResult
  endif

  m = selecMove(position, depth)
  v = -search(position after m, depth + 1)
  Add entry (position, m, depth, v, ...) to TT

  return v
end
*/

struct Node {
  Node *parent = nullptr;
  vector<Node> children;

  int value = 0, visited = 0, depth = 0;

  Node(Node *parent, int depth) : parent(parent), depth(depth) {}
};

Node *expand(Node *current) {
  return nullptr;
}

int test_parsing(const string &fname);

int main(int argc, const char **argv) {
  cout << "MCTS" << endl;

  if (argc != 2) {
    cerr << "Required input arg missing!" << endl;
    return 1;
  }

  return test_parsing(argv[1]);
}

int test_parsing(const string &fname) {
  map_info info;

  if (parse_in_file(fname, info)) {
    cerr << "Failed to parse input" << endl;
    return 1;
  }
  cout << "There are " << info.nr_params << " parameters" << endl;
  cout << "There are " << info.nr_variable_params << " variable parameters"
       << endl;
  cout << "Binary name: " << info.bin_name << endl;

  return 0;
}
