#include <vector>
#include <string>
#include <iostream>
#include "parse_in_file.hpp"

using namespace std;

int* exhaustive_search(void)
{
	arg_range *arg_value_ranges = nullptr;
	int n = mapInfo.nr_variable_params;
	int *arg_values = new int[n];
	int *best_values = new int[n];
	double current_best;

	init_arg_ranges(arg_value_ranges);
	for (int i=0; i<n; i++) {
		arg_values[i] = arg_value_ranges[i].loBound;
	}

	current_best = setArgAndRun(arg_values);
	copy_best_values (n, best_values, arg_values);

	while (1) {
		int ind = n-1;
		double time;

		while (ind >= 0) {
			arg_values[ind] += arg_value_ranges[ind].step;
			if (arg_values[ind] > arg_value_ranges[ind].hiBound) {
				arg_values[ind] = arg_value_ranges[ind].loBound;
				ind--;
			} else
				break;
		}

		if (ind >= 0) {
			time = setArgAndRun(arg_values);
			if (time < current_best) {
				current_best = time;
				copy_best_values(n, best_values, arg_values);
			}
		} else
			break;
	}

	return best_values;

}

