#include "mcts.h"

#include <algorithm>
#include <cmath>
#include <iostream>

#define dlog(X) do { std::cout << X << std::endl; } while (0)

int Mcts::get_best_move(Game &game, int player) {
  Node root(nullptr, 0, opponent(player), 0);
  root.state = game.get_board(); // copy game's board to node's
  int start_player = player;

  // Four phases: decent, roll-out, update and growth done iteratively X times
  for (int i = 0; i < 1000; ++i) {
    Node *current = selection(&root, game);

    if (!current) {
      // dlog("Current is null!");
      continue;
    }

    int value = rollout(current, game, start_player);
    update(current, value);
  }

  if (auto best = best_child_ucb(&root, 0))
    return best->action;
  else
    return -1;
}

Node *Mcts::selection(Node *current, Game &game) {
  while (current && !Game::is_terminal(current->state)) {
    auto valid_moves = Game::get_valid_moves(current->state);

    if (valid_moves.size() > current->children.size()) {
      return expand(current, game);
    } else {
      current = best_child_ucb(current, 1.44);
    }
  }

  return current;
}

Node *Mcts::best_child_ucb(Node *current, double C) {
  if (!current)
    return nullptr;

  Node *best_child = nullptr;
  double best = -std::numeric_limits<double>::infinity(); // negative infinity
  double ucb1;

  for (auto &child : current->children) {
    ucb1 = (static_cast<double>(child->value) /
            static_cast<double>(child->visits)) +
           C * std::sqrt((2.0 * std::log(current->visits)) /
                         static_cast<double>(child->visits));
    if (ucb1 > best) {
      best_child = child.get();
      best = ucb1;
    }
  }

  return best_child;
}

Node *Mcts::expand(Node *current, Game &game) {
  game.set_board(current->state);

  auto valid_moves = Game::get_valid_moves(current->state);

  for (auto m : valid_moves) {
    // Check whether we have already evaluated this move
    auto found_move =
        std::find_if(std::begin(current->children), std::end(current->children),
                     [&](Node::child_ptr &n) { return m == n->action; });
    if (found_move != std::end(current->children))
      continue;

    int acting_player = opponent(current->player_action);
    Node::child_ptr node(
        new Node(current, m, acting_player, current->depth + 1));
    current->children.push_back(node);

    // // Do the move in the game and save it to the child node
    game.mark(acting_player, m);
    node->state = game.get_board();
    game.set_board(current->state);

    return node.get();
  }

  return nullptr;
}

int Mcts::rollout(Node *current, Game &game, int start_player) {
  if (!current)
    return 0;

  game.set_board(current->state);

  // if this move is terminal and the opponent wins, this means we have
  // previously made a move where the opponent can always find a move to win
  // which is not good
  if (current->parent && (game.get_winner() == opponent(start_player))) {
    current->parent->value = std::numeric_limits<int>::min();
    return 0;
  }

  int player = opponent(current->player_action);

  // Do the policy until a winner is found for the first (change?) node added
  while (!game.done()) {
    auto valid_moves = game.get_valid_moves();
    int rmove = game.random_move(player == 1);
    if (rmove < 0) break;
    game.mark(player, rmove);
    player = opponent(player);
  }

  if (game.get_winner() == start_player || game.is_draw())
    return 1;

  return 0;
}

void Mcts::update(Node *current, int value) {
  if (!current)
    return;

  Node *node = current;
  do {
    node->visits++;
    node->value += value;
    node = node->parent;
  } while (node);
}
