// Tic Tac Toe game with MCTS search

#include <iostream>

#include "game.h"
#include "mcts.h"

void test_evaluate() {
  Game game;
  Game::board_t board{{0, 0, 0, 0, 0, 0, 0, 0, 0}};
  game.set_board(board);
  std::cout << game.evaluate(1) << std::endl;
  std::cout << game.evaluate(2) << std::endl;

  board = {{0, 0, 0,
            0, 0, 0,
            0, 0, 0}};
  game.set_board(board);
  std::cout << game.evaluate(1) << std::endl;
  std::cout << game.evaluate(2) << std::endl;

  for (int i = 1; i < 20; ++i) {
    auto res = game.minimax(i, 1, 2);
    std::cout << "Depth: " << i << ", Value: " << std::get<0>(res)
              << ", Move: " << std::get<1>(res) << std::endl;
  }
}

int main() {
  std::cout << "Tic Tac Toe" << std::endl;

  // test_evaluate();
  // return 0;

  Game game;
  // Mcts mcts;
  // game.set_suggest_fn([&](Game &g, int p) { return mcts.get_best_move(g, p);
  // });

  game.set_minimax_suggest(false);

  // game.play_manual();
  game.play_auto();
}
