#pragma once

// Tic Tac Toe game with MCTS search

#include <array>
#include <functional>
#include <iostream>
#include <sstream>
#include <vector>

#include "../random_selector.hpp"

class Game {
public:
  typedef std::array<int, 9> board_t;
  typedef std::function<int(Game &, int)> suggest_move_fn;

  bool mark(int player, int index);
  void unmark(int index) {
    if (index >= 0 && index < 9)
      board[index] = 0;
  }

  void display() const;
  int get_winner() const { return get_winner(this->board); }

  board_t get_board() const { return board; }
  void set_board(const board_t &board) { this->board = board; }

  bool is_draw() const {
    return get_winner() == 0 && get_valid_moves().size() == 0;
  }

  bool done() const { return is_terminal(this->board); }

  std::vector<int> get_valid_moves() const {
    return get_valid_moves(this->board);
  }

  void play_manual();
  void play_auto();
  void play_auto(int player);
  void play_random(bool wait);

  static std::vector<int> get_valid_moves(const board_t &board);
  static bool is_terminal(const board_t &board);
  static int get_winner(const board_t &board);

  int random_move(bool first_player);

  void set_suggest_fn(suggest_move_fn fn) { suggest_fn = fn; }

  int evaluate(int player);

  std::pair<int, int> minimax(int depth, int player, int opponent);
  std::pair<int, int> minimax(int depth, int player, int lbound, int ubound);

  void set_minimax_suggest(bool s) { minimax_suggest = s; }

private:
  board_t board{{0}};
  random_selector<> selector{};
  suggest_move_fn suggest_fn;
  bool minimax_suggest;

  bool make_move_manual(bool first_player);

  int get_first_squares() const { return get_count_squares_with(1); }
  int get_second_squares() const { return get_count_squares_with(2); }

  int get_count_squares_with(int val) const;

  void display_suggestion(int player);
};


inline int opponent(int player) {
  if (player == 1)
    return 2;
  return 1;
}
