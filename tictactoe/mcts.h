#pragma once

#include <iostream>
#include <sstream>
#include <vector>
#include <memory>

#include "../random_selector.hpp"
#include "game.h"

// MCTS implementation
struct Node {
  typedef std::shared_ptr<Node> child_ptr;

  Node *parent;
  std::vector<child_ptr> children;
  int value, visits, action, player_action, depth;
  bool ignore;

  Game::board_t state{{0}};

  Node(Node *parent, int action, int player_action, int depth)
      : parent(parent), action(action), player_action(player_action),
        depth(depth) {}

  std::string to_str() const {
    if (!parent)
      return "Root Node";

    std::stringstream ss;
    ss << "Action: " << action << "Vi/Va: " << visits << "/" << value
       << " (Took action: p" << player_action << ") depth: " << depth;
    return ss.str();
  }
};

class Mcts {
public:
  int get_best_move(Game &game, int player);

  Node *selection(Node *current, Game &game);
  Node *best_child_ucb(Node *current, double C);

  // Expand a node by creating a new move and returning the node
  Node *expand(Node *current, Game &game);

  // Simulate a game with a given policy and return the value
  int rollout(Node *current, Game &game, int start_player);

  void update(Node *current, int value);
};
