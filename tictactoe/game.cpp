#include "game.h"

using namespace std;

inline std::string board_square(int val) {
  if (val == 0)
    return "[ ]";
  if (val == 1)
    return "[X]";
  return "[O]";
}

inline int Game::get_count_squares_with(int val) const {
  int count = 0;
  for (int i = 0; i < 9; ++i) {
    if (board[i] == val)
      ++count;
  }
  return count;
}

std::vector<int> Game::get_valid_moves(const board_t &board) {
  vector<int> moves;

  for (int i = 0; i < 9; ++i) {
    // if the current square is not occupied, then we can make a move
    if (board[i] == 0)
      moves.push_back(i);
  }

  return moves;
}

inline bool Game::is_terminal(const board_t &board) {
  return get_winner(board) != 0 || get_valid_moves(board).size() == 0;
}

inline bool Game::mark(int player, int index) {
  if (index < 0 || index > 8 || board[index] != 0)
    return false;

  if (player != 1 && player != 2)
    return false;

  board[index] = player;
  return true;
}

void Game::display() const {
  for (int i = 0; i < 9; ++i) {
    cout << board_square(board[i]);
    if ((i + 1) % 3 == 0)
      cout << endl;
  }
}

int Game::get_winner(const board_t &board) {
  for (int p = 1; p <= 2; ++p) {
    // Check rows
    if (board[0] == p && board[1] == p && board[2] == p)
      return p;
    if (board[3] == p && board[4] == p && board[5] == p)
      return p;
    if (board[6] == p && board[7] == p && board[8] == p)
      return p;

    // Check columns
    if (board[0] == p && board[3] == p && board[6] == p)
      return p;
    if (board[1] == p && board[4] == p && board[7] == p)
      return p;
    if (board[2] == p && board[5] == p && board[8] == p)
      return p;

    // Check diagonals
    if (board[0] == p && board[4] == p && board[8] == p)
      return p;
    if (board[2] == p && board[4] == p && board[6] == p)
      return p;
  }

  return 0;
}

void Game::play_manual() {
  bool first_player = true;

  for (;;) {
    display();

    if (done()) {
      int winner = get_winner();
      if (winner == 1)
        cout << "First player wins in " << get_first_squares() << " moves!"
             << endl;
      else if (winner == 2)
        cout << "Second player wins in " << get_second_squares() << " moves!"
             << endl;
      else
        cout << "Game ended in a draw!" << endl;
      return;
    }

    if (!make_move_manual(first_player)) {
      cout << "Bye!" << endl;
      return;
    }

    first_player = !first_player;
  }
}

void Game::play_auto() {
  int player = 1;
  std::string input;

  for (;;) {
    std::cout << "Choose player - enter 1 or 2: ";
    try {
      std::getline(std::cin, input);
      player = std::stoi(input);

      if (player == 1 || player == 2) {
        break;
      } else {
        std::cout << "Invalid input" << std::endl;
        continue;
      }
    } catch (const std::invalid_argument &) {
      std::cout << "Invalid input" << std::endl;
      continue;
    }
  }

  std::cout << "You are playing as " << player << " player" << std::endl;
  play_auto(player);
}

void Game::play_auto(int player) {
  int computer = (player == 1) ? 2 : 1;
  int current_player = 1;

  for (;;) {
    display();

    if (done()) {
      int winner = get_winner();
      if (winner == player)
        cout << "You won in " << get_first_squares() << " moves!" << endl;
      else if (winner == computer)
        cout << "Computer won in " << get_second_squares() << " moves!" << endl;
      else
        cout << "Game ended in a draw!" << endl;
      return;
    }

    // This is the manual part
    if (current_player == player) {
      if (!make_move_manual(player == 1)) {
        cout << "Bye!" << endl;
        return;
      }
    } else {
      // if (suggest_fn) {
      //   Game dummy_game = *this;

      //   int s = suggest_fn(dummy_game, computer);
      //   if (s >= 0) { // suggestions can fail
      //     this->mark(computer, s);
      //     cout << "Computer played: " << (s + 1) << endl;
      //   } else {
      //     cout << "Suggestion failed!" << endl;
      //     return;
      //   }
      // } else {
      //   cout << "Error: cannot play computer without suggestion function!"
      //        << endl;
      //   return;
      // }

      auto res = minimax(50, computer, computer);
      int s = std::get<1>(res);
      this->mark(computer, s);
      cout << "Computer played: " << (s + 1) << endl;
    }

    current_player = (current_player == 1) ? 2 : 1;
  }
}

void Game::play_random(bool wait) {
  cout << "Playing the game randomly" << endl;

  bool first_player = get_first_squares() <= get_second_squares();

  for (;;) {
    display();

    if (done()) {
      int winner = get_winner();
      if (winner == 1)
        cout << "First player wins in " << get_first_squares() << " moves!"
             << endl;
      else if (winner == 2)
        cout << "Second player wins in " << get_second_squares() << " moves!"
             << endl;
      else
        cout << "Game ended in a draw!" << endl;
      return;
    }

    if (first_player)
      cout << "First player random move..." << (wait ? "" : "\n");
    else
      cout << "Second player random move..." << (wait ? "" : "\n");

    if (wait)
      getchar();

    random_move(first_player);

    first_player = !first_player;
  }
}

int Game::random_move(bool first_player) {
  int player = first_player ? 1 : 2;
  vector<int> moves = get_valid_moves();
  auto move = selector(moves.begin(), moves.end());

  if (move != moves.end() && mark(player, *move)) {
    return *move;
  }

  return -1;
}

void Game::display_suggestion(int player) {
  if (suggest_fn) {
    Game dummy_game = *this;

    int s = suggest_fn(dummy_game, player);
    if (s >= 0) { // suggestions can fail
      cout << "Suggested move is: " << (s + 1) << endl;
    }

  }

  if (minimax_suggest) {
    auto res = minimax(10, player, opponent(player));
    int s = std::get<1>(res) + 1;
    cout << "Minimax suggests: " << s << std::endl;
  }

}

bool Game::make_move_manual(bool first_player) {
  int player = first_player ? 1 : 2;
  string move;
  int pos;

  for (;;) {
    display_suggestion(player);

    cout << "Enter move (player " << player
         << (first_player ? " [X]): " : " [O]): ");
    std::getline(cin, move);

    if (move == "q" || move == "quit" || move == "exit") {
      return false;
    }

    if (move == "s" || move == "show") {
      cout << "Valid moves: ";
      for (auto &m : get_valid_moves()) {
        cout << (m + 1) << " ";
      }
      cout << endl;
      continue;
    }

    if (move == "r" || move == "random") {
      int rmove = random_move(first_player);
      if (rmove >= 0) {
        cout << "Filled " << (rmove + 1) << " square (randomly)" << endl;
        return true;
      } else {
        cout << "Error: random move failed!" << endl;
        return false;
      }
    }

    if (move == "a" || move == "auto") {
      play_random(true);
      return false;
    }

    if (move == "A" || move == "AUTO") {
      play_random(false);
      return false;
    }

    try {
      pos = std::stoi(move);
    } catch (const std::exception &e) {
      cout << "Invalid input!" << endl;
      continue;
    }

    if (pos >= 1 && pos <= 9) {
      if (mark(player, pos - 1))
        return true;
      else
        cout << "Invalid move as square is already filled" << endl;
    } else {
      cout << "Invalid move. Enter 1..9 or 'q' to exit" << endl;
    }
  }
}

inline int eval_line(int player, Game::board_t &board, int a, int b, int c) {
  int score = 0;
  int val1 = board[a], val2 = board[b], val3 = board[c];
  int opponent = player == 1 ? 2 : 1;

  if (val1 == player) {
    score += 1;
  } else if (val1 == opponent) {
    score -= 1;
  }

  if (val2 == player) {
    if (score == 1)
      score = 10;
    else if (score == -1)
      return 0;
    else
      score = 1;
  } else if (val2 == opponent) {
    if (score == -1)
      score = -10;
    else if (score == 1)
      return 0;
    else
      score = -1;
  }

  if (val3 == player) {
    if (score > 0)
      score *= 10;
    else if (score < 0)
      return 0;
    else
      score = 1;
  } else if (val2 == opponent) {
    if (score < 0)
      score *= 10;
    else if (score > 1)
      return 0;
    else
      score = -1;
  }

  return score;
}

/** The heuristic evaluation function for the current board
    @Return +100, +10, +1 for EACH 3-, 2-, 1-in-a-line for computer.
    -100, -10, -1 for EACH 3-, 2-, 1-in-a-line for opponent.
    0 otherwise   */

/*
 0 1 2
 3 4 5
 6 7 8
 */
int Game::evaluate(int player) {
  int score = 0;
  // rows
  score += eval_line(player, board, 0, 1, 2);
  score += eval_line(player, board, 3, 4, 5);
  score += eval_line(player, board, 6, 7, 8);
  // columns
  score += eval_line(player, board, 0, 3, 6);
  score += eval_line(player, board, 1, 4, 7);
  score += eval_line(player, board, 2, 5, 8);
  // diagonals
  score += eval_line(player, board, 0, 4, 8);
  score += eval_line(player, board, 2, 4, 6);

  return score;
}

std::pair<int, int> Game::minimax(int depth, int player, int computer) {
  int my_seed = computer;
  int opp_seed = opponent(computer);

  int best_score = (player == my_seed) ? std::numeric_limits<int>::min()
                                       : std::numeric_limits<int>::max();
  int current_score, best_move = -1;

  auto valid_moves = get_valid_moves();

  if (valid_moves.empty() || depth <= 0) {
    best_score = evaluate(my_seed);
  } else {
    for (auto m : valid_moves) {
      mark(player, m);

      if (player == my_seed) { // computer is maximising player
        current_score = std::get<0>(minimax(depth - 1, opp_seed, computer));
        if (current_score > best_score) {
          best_score = current_score;
          best_move = m;
        }
      } else { // playser is minimising player
        current_score = std::get<0>(minimax(depth - 1, my_seed, computer));
        if (current_score < best_score) {
          best_score = current_score;
          best_move = m;
        }
      }

      unmark(m);
    }
  }

  return std::make_pair(best_score, best_move);
}

std::pair<int, int> Game::minimax(int depth, int player, int lbound, int ubound) {
  return std::make_pair(0, 0);
}
