#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "parse_in_file.hpp"

using namespace std;

extern map_info mapInfo;

int parse_in_file(const std::string &fileName, map_info &info) {
  ifstream in_file(fileName);
  if (!in_file)
    return 1;

  string line;
  // int arg_ind = 0;

  getline(in_file, info.bin_name);
  info.nr_params = 0;
  while (getline(in_file, line)) {
    stringstream strm(line);
    string tmp, tmp2, tmp3, tmp4;
    int step;
    getline(strm, tmp, ' ');
    stringstream strm2(tmp);
    getline(strm2, tmp2, '-');
    getline(strm2, tmp3);
    if (tmp3.empty()) {
      argument arg(tmp);
      info.args.push_back(arg);
    } else {
      if (tmp2 != tmp3) {
        argument arg;
        arg.setVariable(true);
        arg.setLoBound(atoi(tmp2.c_str()));
        arg.setHiBound(atoi(tmp3.c_str()));
        getline(strm, tmp4);
        if ((step = atoi(tmp4.c_str())) > 0)
          arg.setStep(step);
        else
          arg.setStep(1);
        info.args.push_back(arg);
        info.nr_variable_params++;
      } else {
        argument arg(atoi(tmp2.c_str()));
        info.args.push_back(arg);
      }
    }
    info.nr_params++;
  }

  return 0;
}

void parse_in_file(string fileName) {
  ifstream in_file(fileName);
  string line;
  // int arg_ind = 0;

  getline(in_file, mapInfo.bin_name);
  mapInfo.nr_params = 0;
  while (getline(in_file, line)) {
    stringstream strm(line);
    string tmp, tmp2, tmp3, tmp4;
    int step;
    getline(strm, tmp, ' ');
    stringstream strm2(tmp);
    getline(strm2, tmp2, '-');
    getline(strm2, tmp3);
    if (tmp3.empty()) {
      argument arg(tmp);
      mapInfo.args.push_back(arg);
    } else {
      if (tmp2 != tmp3) {
        argument arg;
        arg.setVariable(true);
        arg.setLoBound(atoi(tmp2.c_str()));
        arg.setHiBound(atoi(tmp3.c_str()));
        getline(strm, tmp4);
        if ((step = atoi(tmp4.c_str())) > 0)
          arg.setStep(step);
        else
          arg.setStep(1);
        mapInfo.args.push_back(arg);
        mapInfo.nr_variable_params++;
      } else {
        argument arg(atoi(tmp2.c_str()));
        mapInfo.args.push_back(arg);
      }
    }
    mapInfo.nr_params++;
  }
}

double setArgAndRun(int arg_val[]) {
  std::string commandToRun = "./" + mapInfo.bin_name;
  int j = 0;
  for (int i = 0; i < mapInfo.nr_params; i++) {
    if (mapInfo.args[i].getVariable()) {
      commandToRun += to_string(arg_val[j]);
      j++;
    } else {
      if (mapInfo.args[i].getConstValue().empty()) {
        commandToRun += to_string(mapInfo.args[i].getLoBound());
      } else {
        commandToRun += mapInfo.args[i].getConstValue();
      }
    }
  }
  char buffer[128];
  std::shared_ptr<FILE> pipe(popen(commandToRun.c_str(), "r"), pclose);
  string last_line;
  if (!pipe)
    throw std::runtime_error("popen() failed!");
  while (!feof(pipe.get())) {
    if (fgets(buffer, 128, pipe.get()) != NULL) {
      last_line = buffer;
    }
  }
  return std::stod(last_line);
}

void init_arg_ranges(arg_range a[]) {
  a = new arg_range[mapInfo.nr_variable_params];
  int ind = 0;
  for (int j = 0; j < mapInfo.nr_params; j++) {
    if (mapInfo.args[j].getVariable()) {
      a[ind].loBound = mapInfo.args[j].getLoBound();
      a[ind].hiBound = mapInfo.args[j].getHiBound();
      a[ind].step = mapInfo.args[j].getStep();
    }
  }
}

void copy_best_values(int n, int best_values[], int arg_values[]) {
  for (int i = 0; i < n; i++) {
    best_values[i] = arg_values[i];
  }
}
