#ifndef RP_PARFOR_NUMA
#define RP_PARFOR_NUMA

#include <tbb/tbb.h>

typedef struct {
  void *p_data;
  unsigned int size;
} data_t;

template<class R>
class FarmReplicationClass {
private:
  std::vector< std::vector<void*> > data_vectors;
  std::function<void(int,std::vector<void*>,R*)> worker_function;
  int cores_per_node;
  int chunk_size;
  R *res_array;
public:

  void operator()(const tbb::blocked_range<size_t>& r) const {
    int node = (r.begin() / chunk_size) / cores_per_node;
    numa_run_on_node(node);
    for (size_t i=r.begin(); i!=r.end(); i++) {
      fprintf (stderr, "Vector of node %d is %p\n", node, data_vectors[node]);
      worker_function (i, data_vectors[node], res_array);
    }
  }

  FarmReplicationClass(std::function<void(int,std::vector<void*>,R*)> f,
		       int nr_cpu_w,
		       std::vector<data_t> a,
		       R *r,
		       int chunk_size) : chunk_size(chunk_size) {
    cores_per_node = numa_num_configured_cpus()/numa_num_configured_nodes();
    int num_copies = (nr_cpu_w - 1) / cores_per_node + 1;
    int array_len = a.size();
    int *size;
    data_vectors = std::vector<std::vector<void*>>(num_copies);
    for (int i=0; i<num_copies; i++) {
      data_vectors[i] = std::vector<void*>(array_len);
      for (int j=0; j<array_len; j++) {
	if (i>0) {
	  data_vectors[i][j] = (void *) numa_alloc_onnode(a[j].size, i);
	  memcpy(data_vectors[i][j], a[j].p_data, a[j].size);
	} else {
	  data_vectors[i][j] = a[j].p_data;
	}
      }
    }
    worker_function = f;
    res_array = r;
    fprintf (stderr, "Data vectors %p\n", data_vectors);
  };

  FarmReplicationClass(std::function<void(int,std::vector<void*>,R*)> f,
		       int nr_cpu_w,
		       std::vector<data_t> a,
		       R *r,
		       int chunk_size,
		       std::vector< std::vector<void*> > *data) {

    FarmReplicationClass(f, nr_cpu_w, a, r, chunk_size);
    *data = data_vectors;
    fprintf (stderr, "Konjak %p\n", data_vectors);
  }

  FarmReplicationClass(std::function<void(int,std::vector<void*>,R*)> worker_function,
		       int nr_cpu_w,
		       std::vector< std::vector<void*> > data,
		       R *res_array,
		       int chunk_size) :
    worker_function(worker_function), data_vectors(data), chunk_size(chunk_size), res_array(res_array) {
    cores_per_node = numa_num_configured_cpus()/numa_num_configured_nodes();
  }

  std::vector< std::vector<void*> > getData () {
    return data_vectors;
  }
};


#endif // RP_PARFOR_NUMA
