FROM ubuntu:16.10
MAINTAINER Chathura Colombage <cddc@st-andrews.ac.uk>

VOLUME "/project"
WORKDIR "/project"

RUN apt-get update && \
    apt-get dist-upgrade -y

# Add LLVM deb src for 3.9

# RUN echo 'deb http://apt.llvm.org/yakkety/ llvm-toolchain-yakkety-3.9 main' >> /etc/apt/sources.list && \
#     echo 'deb-src http://apt.llvm.org/yakkety/ llvm-toolchain-yakkety-3.9 main' >> /etc/apt/sources.list && \
#     apt-get update && \
#     apt-get install cmake llvm-3.9 clang-3.9 libclang-3.9-dev zlib1g-dev --allow-unauthenticated -y

# We need following simlinks as Unbuntu installation has versioned binaries in
# the default path
# RUN mkdir ~/bin                              && \
#     ln -s $(which clang++-3.9) ~/bin/clang++ && \
#     ln -s $(which clang-3.9) ~/bin/clang     && \
#     ln -s $(which clang-3.9) ~/bin/gcc       && \
#     ln -s $(which llvm-config-3.9) ~/bin/llvm-config


# For examples we need following packages
RUN apt-get install cmake zlib1g-dev g++ -y

# Update path ~/bin
ENV PATH "$PATH:~/bin"
