#include <vector>
#include <string>
#include <iostream>
#include "parse_in_file.hpp"

using namespace std;

int* random_sampling(int numSamples)
{
	arg_range *arg_value_ranges = nullptr;
	int n = mapInfo.nr_variable_params;
	int *arg_values = new int[n];
	int *best_values = new int[n];
	double current_best;

	init_arg_ranges(arg_value_ranges);
	for (int i=0; i<n; i++) {
		arg_values[i] = arg_value_ranges[i].loBound;
	}

	current_best = setArgAndRun(arg_values);
	copy_best_values (n, best_values, arg_values);

	for (int i=0; i<numSamples; i++) {
		for (int j=0; j<n; j++) {
			int range = (arg_value_ranges[j].hiBound - arg_value_ranges[j].loBound) / arg_value_ranges[j].step;
			int randStep = rand() % arg_value_ranges[j].step;
			arg_values[j] = arg_value_ranges[j].loBound + randStep * arg_value_ranges[j].step;
		}

		double time = setArgAndRun(arg_values);
		if (time < current_best) {
			current_best = time;
			copy_best_values(n, best_values, arg_values);
		}
	}


	return best_values;

}
