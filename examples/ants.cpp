#include <iostream>

// This is the ant colony example for mock testing MCTS algorithm. This program
// should accept same inputs as the real program and output values for different
// configurations in a consistent manner.

using namespace std;

int main() {
  cout << "Ants are everywhere!" << endl;
}
