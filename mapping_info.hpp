#ifndef RP_MAPPING_INFO_H
#define RP_MAPPING_INFO_H

#include <vector>
#include <string>

class argument {
private:
  bool isVariable;
  int loBound;
  int hiBound;
  int step;
  std::string constVal;
public:
  argument(int val) : isVariable(false), loBound(val), hiBound(val) { constVal.empty(); };
  argument(std::string val) : isVariable(false), constVal(val) {};
  argument() : isVariable(true) {};


  std::string getConstValue () { return constVal; }
  void setVariable (bool x) { isVariable = x; }

  bool getVariable () { return isVariable; }

  void setLoBound (int val) { loBound = val; }

  int getLoBound () { return loBound; }

  void setHiBound (int val) { hiBound = val; }

  int getHiBound () { return hiBound; }

  void setStep (int val) { step = val; }

  int getStep () { return step; }
};

struct map_info {
  int nr_params;
  int nr_variable_params;
  std::string bin_name;
  std::vector<argument> args;
};

typedef struct {
	int loBound;
	int hiBound;
	int step;
} arg_range;

#endif // RP_MAPPING_INFO_H
